import React, { Component } from 'react'
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom'
import GuardRoutes from './Container/GuardRoute'

import RegisterPage from './Container/OnBoarding/RegisterPage'
import LoginPage from './Container/OnBoarding/LoginPage'
import HomePage from './Container/HomePage'
import MovieDetailPage from './Container/DetailPage/movieDetailPage'
import ProfilePage from './Container/Account/ProfilePage'
import MoviePage from './Container/MoviePage'
import TvshowPage from './Container/TvshowPage'
import UpComingMovie from './Container/Category/movie/UpComingMovie'
import PlayingMovie from './Container/Category/movie/PlayingMovie'
import PopMovie from './Container/Category/movie/PopMovie'
import SearchPage from './Container/SearchPage'
import OnAirTvshow from './Container/Category/tvshow/OnAirTvshow'
import PopTvshow from './Container/Category/tvshow/PopTvshow'
import tvshowDetailPage from './Container/DetailPage/tvshowDetailPage'
import PageNotFound from './Container/PageNotFound'

class Routes extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <GuardRoutes exact path='/' component={HomePage} />
          <Route path='/register' component={RegisterPage} />
          <Route path='/login' component={LoginPage} />
          <GuardRoutes path='/profilepage' component={ProfilePage} />
          <GuardRoutes path='/movie/detail/:id' component={MovieDetailPage} />
          <GuardRoutes path='/tvshow/detail/:id' component={tvshowDetailPage} />
          <GuardRoutes path='/movie/upcoming' component={UpComingMovie} />
          <GuardRoutes path='/movie/nowplaying' component={PlayingMovie} />
          <GuardRoutes path='/movie/popular' component={PopMovie} />
          <GuardRoutes path='/movie' component={MoviePage} />
          <GuardRoutes path='/tvshow/onair' component={OnAirTvshow} />
          <GuardRoutes path='/tvshow/popular' component={PopTvshow} />
          <GuardRoutes path='/tvshow' component={TvshowPage} />
          <GuardRoutes path='/search/:query' component={SearchPage} />
          <GuardRoutes path='/404' component={PageNotFound} />
          <Redirect to='/404' />
        </Switch>
      </BrowserRouter>
    )
  }
}
export default Routes