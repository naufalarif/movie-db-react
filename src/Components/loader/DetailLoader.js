import React from "react"
import ContentLoader from "react-content-loader" 

const DetailLoader = () => (
  <ContentLoader 
    speed={2}
    width={700}
    height={450}
    viewBox="0 0 700 450"
    backgroundColor="#373837"
    foregroundColor="#717171"
  >
    <rect x="279" y="72" rx="3" ry="3" width="409" height="326" /> 
    <rect x="6" y="75" rx="5" ry="5" width="232" height="34" /> 
    <rect x="5" y="179" rx="5" ry="5" width="232" height="220" /> 
    <rect x="8" y="126" rx="5" ry="5" width="228" height="30" />
  </ContentLoader>
)

export default DetailLoader