import React from 'react'
import ContentLoader from 'react-content-loader'

const Loader = props => {  

  return (
    <ContentLoader
    speed={2}
    width={400}
    height={350}
    viewBox="0 0 400 350"
    backgroundColor="#373837"
    foregroundColor="#717171"
  >
    <rect x="1" y="2" rx="3" ry="3" width="200" height="350" />
    </ContentLoader>
  )
}

export default Loader