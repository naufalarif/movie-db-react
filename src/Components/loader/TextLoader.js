import React from "react"
import ContentLoader from "react-content-loader" 

const TextLoader = () => (
  <ContentLoader 
    speed={2}
    width={700}
    height={450}
    viewBox="0 0 700 450"
    backgroundColor="#373837"
    foregroundColor="#717171"
  >
    <rect x="1" y="17" rx="3" ry="3" width="342" height="27" />
  </ContentLoader>
)

export default TextLoader