import React, { Component } from 'react'
import CardTvshow from '../card/CardTvshow'
import { Row, Col } from 'antd'

class TvMoreList extends Component {
  render() {
    const { payload } = this.props
    return (
      <div>
        <Row>
        {
          payload.map((data, idx) => {
            return (
              <Col
                xs={24} sm={12} md={8} lg={6} xl={6}
                className='p-3'
                key={idx}
              >
                <CardTvshow payload={data} />    
              </Col>
            )
          })
        }
        </Row>
      </div>
    )
  }
}

export default TvMoreList
