import React, { Component } from 'react'
import Slider from 'react-slick'
import _ from 'lodash'
import { Row, Col } from 'antd'
import CardMyMovieList from '../../card/CardMyMovieList'

class MyMovieList extends Component {
  render() {
    // set data here
    const { payload } = this.props
    const data = !_.isEmpty(payload) ? payload : []

    // logic here
    const slidesToShow = window.innerWidth <= 450 ? 1 : window.innerWidth <= 830 ? 2 : 4
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: slidesToShow,
      slidesToScroll: 1
    }

    // console.log(payload)
    // logic not allowed in return!
    return (
      <div>
        {
          data.length > 4 ? (
            <Slider {...settings}>
              {
                data.map((data, idx) => {
                  return (
                    <div className='p-3' key={idx}>
                      <CardMyMovieList payload={data} />
                    </div>
                  )
                })
              }
            </Slider>
          ): (
            <Row>
              {
                data.map((data, idx) => {
                  return (
                    <Col xs={12} sm={12} md={7} lg={5} xl={5} key={idx}>
                      <CardMyMovieList payload={data} />
                    </Col>
                  )
                })
              }
            </Row>
          )
        }
      </div>
    )
  }
}

export default MyMovieList
