import React, { Component } from 'react'
import { withRouter } from 'react-router'
import Slider from 'react-slick'
import _ from 'lodash'
import CardMovieCategory from '../../card/CardMovieCategory'

class CategoryList extends Component {
  render() {
    // set data here
    const { payload } = this.props

    // logic here
    const lengthSlicker = payload.length >= 5 ? 5 : payload.length
    const lengthMap = payload.length > 7 ? 7 : payload.length
    const slidesToShow = window.innerWidth <= 450 ? 1 :  window.innerWidth <= 830 ? 3 : lengthSlicker
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: slidesToShow,
      slidesToScroll: 1
    }
    
    // logic not allowed in return!
    return (
      !_.isEmpty(payload) ? <Slider {...settings}>
        {
          payload.map((data, idx) => {
            return(
              <div className='pl-3 pr-3' key={idx}>
                <CardMovieCategory payload={data} />
              </div>
            )
          })
        } </Slider>
      : (
        <div> Maaf tidak ada</div>
      )
    )
  }
}

export default withRouter(CategoryList)
