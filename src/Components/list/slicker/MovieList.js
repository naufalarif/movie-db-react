import React, { Component } from 'react'
import Slider from 'react-slick'
import CardMovieSlicker from '../../card/CardMovieSlicker'

class MovieList extends Component {
  render() {
    // set data here
    const { payload } = this.props
    
    // logic here
    const slidesToShow = window.innerWidth <= 450 ? 1 : window.innerWidth <= 830 ? 2 : 4
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: slidesToShow,
      slidesToScroll: 1
    }

    // logic not allowed in return!
    return (
      <div>
        <Slider {...settings}>
          {
            payload.slice(0,6).map((data, idx) => {
              return (
                <div className='p-3' key={idx}>
                  <CardMovieSlicker payload={data} />
                </div>
              )
            })
          }
        </Slider>
      </div>
    )
  }
}

export default MovieList
