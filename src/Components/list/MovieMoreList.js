import React, { Component } from 'react'
import CardMovie from '../card/CardMovie'
import { Row, Col } from 'antd'

class MovieMoreList extends Component {
  render() {
    const { payload } = this.props
    return (
      <div>
        <Row>
        {
          payload.map((data, idx) => {
            return (
              <Col
                xs={24} sm={12} md={8} lg={6} xl={6}
                className='p-3'
                key={idx}
              >
                <CardMovie payload={data} />    
              </Col>
            )
          })
        }
        </Row>
      </div>
    )
  }
}

export default MovieMoreList
