import React, { Component } from 'react'
import { withRouter } from 'react-router'
import _ from 'lodash'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,  
  NavbarText,
  Row,
  Col
} from 'reactstrap'

// Img
import Logo from '../../static/assets/logo.png'

// Redux
import * as ACTION from '../../Redux/action/searchAction'
import { connect } from 'react-redux'

class NavigationBar extends Component {  
  constructor(props) {
    super(props)
    this.state = {
      type: '',
      query: '',
      isOpen: false,
      isActive: false
    }
  }

  toggle = () => {
    this.setState({
      isOpen: !this.state.isOpen,
      isActive: false
    })
  }

  search = () => {
    this.setState({
      isActive: !this.state.isActive,
      isOpen: false
    })
  }

  handleSignOut = () => {
    const { history } = this.props
    localStorage.removeItem('token')
    history.push('/login')
  }  

  handleChange = (e) => {
    this.setState({ query: e.target.value })
  }

  handleClick = () => {    
    const { query } = this.state
    if(!_.isEmpty(query)) {
      this.props.getSearchQuery(query)
      this.props.history.push({
        pathname: `/search/${query}`,
        state: { query: query }
      })
    }
  }

  // handlePressed = e => {
  //   if(e.keyCode === 13) {
  //     this.handleClick()
  //   }
  // }

  render() {
    const { history, home, movie, tvshow } = this.props
    const { isOpen, isActive } = this.state
    return (      
      <div>
        <Navbar color="dark" light expand="md">
          <NavbarToggler onClick={() => this.toggle()} />
          <NavbarBrand href="/">
            <img
              src={Logo} alt='...'
              width='120'
            />
          </NavbarBrand>
          <NavbarToggler onClick={() => this.search()} />          
          <Collapse isOpen={isOpen} navbar>
            <Nav className="mr-auto" navbar>
              <NavItem>
                <NavLink 
                  className={home}
                  style={{ fontWeight: 'bolder' }}
                  onClick={() => history.push('/')}
                >
                  Home
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink 
                  className={tvshow}
                  style={{ fontWeight: 'bolder' }}
                  onClick={() => history.push('/tvshow')}
                >
                  Tv Show
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink 
                  className={movie}
                  style={{ fontWeight: 'bolder' }}
                  onClick={() => history.push('/movie')}
                >
                  Movie
                </NavLink>
              </NavItem>              
            </Nav>
            <NavbarText>
              <Collapse isOpen={false} navbar>
                <input 
                  className="form-control" 
                  type="text" 
                  placeholder="Search..." 
                  onChange={(e) => this.handleChange(e)}                 
                />
                <button className="search-navbar pr-4" onClick={() => this.handleClick()} >
                  <img className="fas fa-search" src="fas fa-search" alt=''/>            
                </button>
              </Collapse>
            </NavbarText>            
            <Nav>
              <NavItem>
                <NavLink  
                  className='nav-logout' 
                  style={{ fontWeight: 'bolder' }}
                  onClick={() => this.handleSignOut()}
                >
                  Logout
                </NavLink>
              </NavItem>
            </Nav>
          </Collapse>      

          {/* -------- Responsive ----- */}
          <Collapse isOpen={isActive}>
            <Row justify="center">              
              <Col xs={10} sm={4} md={12} lg={14} xl={6}>
                <input className="form-control" type="text" placeholder="Search..." onChange={(e) => this.handleChange(e)} />
              </Col>
              <Col xs={2} sm={4} md={8} lg={8} xl={6}>
                <button className="search-icon" onClick={() => this.handleClick()} >
                  <img className="fas fa-search" src="fas fa-search" alt=''/>            
                </button>
              </Col>
            </Row>
          </Collapse>
        </Navbar>
      </div>    
    )    
  }
}

const mapStateToProps = state => ({
  
})

const mapDispatchToProps = dispatch => ({
  getSearchQuery: query => dispatch(ACTION.getSearchQuery(query))
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(NavigationBar))