import React, { Component } from 'react'
import { Row, Col } from 'antd'

class NavigationGenre extends Component {
  render() {
    const { payload, onClick } = this.props
    return (
      <div>
        <Row>
          {
            payload.map((data, idx) => {
              return (
                <Col xs={10} sm={10} md={4} lg={4} xl={2} 
                  key={idx} span={3} onClick={onClick}>
                  <div className='category-container'>
                    <span>{data.name}</span>
                  </div>
                </Col>
              )
            })
          }
        </Row>
      </div>
    )
  }
}

export default NavigationGenre
