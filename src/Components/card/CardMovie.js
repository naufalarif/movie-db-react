import React, { Component } from 'react'
import { withRouter } from 'react-router'
import _ from 'lodash'

// Comp
// import ImageLoader from '../loader/ImageLoader'

// Icon
import Star from '../../static/assets/star.svg'

// Utils
import config from '../../config'
import { GenreGenerator } from '../../Utils/GenreGenerator'

// Antd
import { Row, Col } from 'antd'

class CardMovie extends Component {

  handleClick = (payload) => {
    const title = payload.title.toLowerCase()
    this.props.history.push({
      pathname: `/movie/detail/${title}`,
      state: { payload: payload.id }
    })
  }
 
  render() {
    // set data here
    const { payload } = this.props
    const genrePayload = payload.genre_ids
    const genreArr = []
    const datePayload = !_.isEmpty(payload) ? payload.release_date : ''

    // logic here
    genrePayload.forEach(items => {
      genreArr.push(GenreGenerator(items))
    })
    const genre = genreArr.slice(0,2).toString().replace(/,/g, "  |   ")
    const date = datePayload.substring(0, 4)
    
    // logic not allowed in return!
    return (
      <div onClick={() => this.handleClick(payload)} className='card-custom p-3'>
        <div className='pb-3'>
          <img
            src={`${config.backdropURL}${payload.backdrop_path}`}
            alt='...'
            width='100%'
            style={{ borderRadius: '10px' }}
          />
        </div>
        <div>
          <span style={{ fontSize: '20px', fontWeight: 'bold' }}>{payload.title}</span>
        </div>
        <div>
          <span style={{ color: '#a7a7a7' }}>{date} - {genre}</span>
        </div>
        <div>
          <Row>
            <Col span={1} style={{ marginRight: '5px' }}>
              <img 
                style={{ marginTop: '-5px' }}
                src={Star}
                width='100%'
                alt=''
              />
            </Col>
            <Col>
              <span style={{ color: '#a7a7a7' }}>{payload.vote_average} ({payload.vote_count})</span>
            </Col>
          </Row>
        </div>
      </div>
    )
  }
}

export default withRouter(CardMovie)