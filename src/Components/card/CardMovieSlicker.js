import React, { Component } from 'react'
import { withRouter } from 'react-router'
import _ from 'lodash'

// Icon
import Star from '../../static/assets/star.svg'

// Antd
import { Row, Col } from 'antd'

// Utils
import config from '../../config'
import { GenreGenerator } from '../../Utils/GenreGenerator'

class CardMovieSlicker extends Component {

  handleClick = (payload) => {
    const title = payload.title.toLowerCase()
    this.props.history.push({
      pathname: `/movie/detail/${title}`,
      state: { payload: payload.id }
    })
  }

  render() {
    // set data here
    const { payload } = this.props
    const genrePayload = payload.genre_ids
    const genreArr = []
    
    // logic here
    const date = !_.isEmpty(payload.release_date) ? payload.release_date.substring(0,4) : payload.first_air_date.substring(0,4)
    const title = !_.isEmpty(payload.title) ? payload.title : payload.name
    genrePayload.forEach(items => {
      genreArr.push(GenreGenerator(items))
    })
    const genre = genreArr.slice(0,2).toString().replace(/,/g, "  |   ")
    
    // logic not allowed in return!
    return (
      <div className='p-3 card-custom' onClick={() => this.handleClick(payload)}>
        <div className='pb-3'>
          <img 
            style={{ borderRadius: '10px' }}
            src={`${config.backdropURL}${payload.backdrop_path}`}
            alt=''
            width='100%'
          />
        </div>
        <div>
          <div>
            <span style={{ fontSize: '20px', fontWeight: 'bold' }}>{title}</span>
          </div>
          <div>
            <span style={{ color: '#a7a7a7' }}>{date} - {genre}</span>
          </div>
          <div>
            <Row>
              <Col span={1} style={{ marginRight: '5px' }}>
                <img 
                  style={{ marginTop: '5.5px' }}
                  src={Star}
                  width='100%'
                  alt=''
                />
              </Col>
              <Col>
                <span style={{ color: '#a7a7a7' }}>{payload.vote_average} ({payload.vote_count})</span>
              </Col>
            </Row>
          </div>
        </div>
      </div>
    )
  }
}

export default withRouter(CardMovieSlicker)
