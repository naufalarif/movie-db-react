import React, { Component } from 'react'
import { withRouter } from 'react-router'

// Antd
import { Row, Col } from 'antd'

// Icon
import Star from '../../static/assets/star.svg'
// import ImageLoader from '../loader/ImageLoader'

// Utils
import config from '../../config'
import { GenreGenerator } from '../../Utils/GenreGenerator'

class CardTvshow extends Component {
  constructor(props) {
    super(props)
    this.state = {
      id: 0
    }
  }

  handleClick = (payload) => {
    this.props.history.push({
      pathname: `/tvshow/detail/${payload.title.toLowerCase()}`,
      state: { payload: payload.id }
    })
  }
  
  render() {
  const { payload } = this.props
  const genrePayload = payload.genre_ids
  const genreArr = []

  // logic here
  genrePayload.forEach(items => {
    genreArr.push(GenreGenerator(items))
  })
  const genre = genreArr.slice(0,2).toString().replace(/,/g, "  |   ")
  const date = payload.first_air_date.substring(0,4)

    return (
      <div onClick={() => this.handleClick(payload)} className='card-custom p-3'>
        <div className='pb-3'>
          <img
            src={`${config.backdropURL}${payload.backdrop_path}`}
            alt='...'
            width='100%'
            style={{ borderRadius: '10px' }}
          />
        </div>
        <div>
          <span style={{ fontSize: '20px', fontWeight: 'bold' }}>{payload.name}</span>
        </div>
        <div>
          <span style={{ color: '#a7a7a7' }}>{date} - {genre}</span>
        </div>
        <div>
          <Row>
            <Col span={1} style={{ marginRight: '5px' }}>
              <img 
                style={{ marginTop: '-5px' }}
                src={Star}
                width='100%'
                alt=''
              />
            </Col>
            <Col>
              <span style={{ color: '#a7a7a7' }}>{payload.vote_average} ({payload.vote_count})</span>
            </Col>
          </Row>
        </div>
      </div>
    )
  }
}

export default withRouter(CardTvshow)