import React, { Component } from 'react'
import { withRouter } from 'react-router'
import _ from 'lodash'

// Utils
import config from '../../config'

class CardMovieSlicker extends Component {

  render() {
    // set data here
    const { payload } = this.props
    
    // logic not allowed in return!
    return (
      <div className='p-3 card-custom'>
        <div className='pb-3'>
          <img 
            style={{ borderRadius: '10px' }}
            src={`${config.posterURL}${payload.poster_path}`}
            alt=''
            width='100%'
          />
        </div>
        <div>
          <div>
            <div>
              <span style={{ fontWeight: 'bold' }}>{payload.name}</span>
            </div>
          </div>
          <div>
            <span style={{ color: '#a7a7a7' }}>{payload.episode_count} Episodes</span>
          </div>
        </div>
      </div>
    )
  }
}

export default withRouter(CardMovieSlicker)
