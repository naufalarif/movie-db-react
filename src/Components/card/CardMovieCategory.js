import React, { Component } from 'react'
import { withRouter } from 'react-router'

// Utils
import config from '../../config'
import { GenreGenerator } from '../../Utils/GenreGenerator'

class CardMovieCategory extends Component {

  handleClick = (payload) => {
    const title = payload.title.toLowerCase()
    this.props.history.push({
      pathname: `/movie/detail/${title}`,
      state: { payload: payload.id }
    })
  }

  render() {
    // set data here
    const { payload } = this.props
    const genrePayload = payload.genre_ids
    const genreArr = []

    // logic here
    const title = payload.name
    const date = payload.release_date.substring(0, 4)
    genrePayload.forEach(items => {
      genreArr.push(GenreGenerator(items))
    })
    const genre = genreArr.slice(0,2).toString().replace(/,/g, "  |   ")

    // logic not allowed in return!
    return (
      <div className='p-3 card-category' onClick={() => this.handleClick(payload)}>
        <div className='pb-3'>
          <img
            src={`${config.posterURL}${payload.poster_path}`}
            alt='...'
            width='100%'
            style={{ borderRadius: '10px' }}
          />
        </div>
        <div>
          <span style={{ fontSize: '20px', fontWeight: 'bold' }}>{title}</span>
        </div>
        <div>
          <span style={{ color: '#a7a7a7' }}>{date} - {genre}</span>
        </div>
        <div>
          <span style={{ color: '#a7a7a7' }}>{payload.vote_average} ({payload.vote_count})</span>
        </div>
      </div>
    )
  }
}

export default withRouter(CardMovieCategory)
