import React, { Component } from 'react'
import { withRouter } from 'react-router'
import _ from 'lodash'

import Star from '../../static/assets/star.svg'

// Utils
import config from '../../config'
import { Row, Col } from 'antd'

class CardMyTvList extends Component {

  handleClick = (payload) => {
    this.props.history.push({
      pathname: `tvshow/detail/${payload.name.toLowerCase()}`,
      state: { payload: payload.id }
    })
  }

  render() {
    // set data here
    const { payload } = this.props
    const genrePayload = !_.isEmpty(payload) ? payload.genres.map(genres => genres.name) : []
    const genre = genrePayload.toString().replace(/,/g, "  |   ")
    const date = payload.first_air_date.substring(0,4)
    
    // logic not allowed in return!
    return (
      <div className='p-3 card-custom' onClick={() => this.handleClick(payload)}>
        <div className='pb-3'>
          <img 
            style={{ borderRadius: '10px' }}
            src={`${config.posterURL}${payload.poster_path}`}
            alt=''
            width='100%'
          />
        </div>
        <div>
          <div>
            <span style={{ fontSize: '20px', fontWeight: 'bold' }}>{payload.name}</span>
          </div>
          <div>
            <span style={{ color: '#a7a7a7' }}>{date} - {genre}</span>
          </div>
          <div>
            <Row>
              <Col span={1} style={{ marginRight: '5px' }}>
                <img 
                  style={{ marginTop: '-5px' }}
                  src={Star}
                  width='100%'
                  alt=''
                />
              </Col>
              <Col>
                <span style={{ color: '#a7a7a7' }}>{payload.vote_average} ({payload.vote_count})</span>
              </Col>
            </Row>
          </div>
        </div>
      </div>
    )
  }
}

export default withRouter(CardMyTvList)
