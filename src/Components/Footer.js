import React, { Component } from 'react'

class Footer extends Component {
  render() {
    return (
      <div className='container footer'>
        <span>&copy; Copyright 2020 Moviebase, Inc.</span>
      </div>
    )
  }
}

export default Footer
