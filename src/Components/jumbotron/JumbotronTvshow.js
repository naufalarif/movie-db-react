import React, { Component } from 'react'
import { withRouter } from 'react-router'
import config from '../../config'
import _ from 'lodash'

// Antd & Comp
import { Carousel } from 'antd'
import ImageLoader from '../loader/ImageLoader'

// Redux
import { connect } from 'react-redux'
import * as ACTION from '../../Redux/action/tvshowPopAction'

class JumbotronTvshow extends Component {

  componentDidMount() {    
    this.props.fetchTvshowPop()
  }

  render() {
    const { tvState } = this.props    
    const payload = _.isEmpty(tvState.items) ? [] : tvState.items.results

    const jumboFirst = _.isEmpty(payload) ? <ImageLoader/> : <img className="card-img" src={`${config.backdropURL}${payload[0].backdrop_path}`} alt="..."/>
    const jumboSec = _.isEmpty(payload) ? <ImageLoader/> : <img className="card-img" src={`${config.backdropURL}${payload[1].backdrop_path}`} alt="..."/>
    const jumboThird = _.isEmpty(payload) ? <ImageLoader /> : <img className="card-img" src={`${config.backdropURL}${payload[2].backdrop_path}`} alt="..."/>
    const jumboFourth = _.isEmpty(payload) ? <ImageLoader /> : <img className="card-img" src={`${config.backdropURL}${payload[3].backdrop_path}`} alt="..."/>
    
    return (
      <div>        
        <Carousel autoplay>
          <div>
            {jumboFirst}
          </div>
          <div>
            {jumboSec}
          </div>
          <div>
            {jumboThird}
          </div>
          <div>
            {jumboFourth}
          </div>
        </Carousel>
      </div>
    )
  }
}

const mapStateToProps = state => ({
    tvState: state.tvshowPopReducer
})

const mapDispatchToProps = dispatch => ({   
    fetchTvshowPop: () => dispatch(ACTION.fetchTvshowPop())
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(JumbotronTvshow)) 