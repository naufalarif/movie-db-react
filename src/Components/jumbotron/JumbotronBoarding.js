import React, { Component } from 'react'
import { withRouter } from 'react-router'
import config from '../../config'
import _ from 'lodash'

// Antd & Comp
import { Carousel, Row, Col } from 'antd'
import ImageLoader from '../loader/ImageLoader'

// Redux
import { connect } from 'react-redux'
import * as ACTION_MOVIE from '../../Redux/action/moviePopAction'
import * as ACTION_TVSHOW from '../../Redux/action/tvshowPopAction'

class JumbotronBoarding extends Component {

  componentDidMount() {
    this.props.fetchMoviesPop()
    this.props.fetchTvshowPop()
  }

  render() {
    const { movieState, tvState } = this.props
    const moviePayload = !_.isEmpty(movieState.items) ? movieState.items.results : []
    const tvPayload = _.isEmpty(tvState.items) ? [] : tvState.items.results
    
    const jumboFirst = _.isEmpty(moviePayload) ? <ImageLoader/> : <img className="card-img" src={`${config.backdropURL}${moviePayload[0].backdrop_path}`} alt="..."/>
    const jumboSec = _.isEmpty(moviePayload) ? <ImageLoader/> : <img className="card-img" src={`${config.backdropURL}${moviePayload[1].backdrop_path}`} alt="..."/>
    const jumboThird = _.isEmpty(tvPayload) ? <ImageLoader /> : <img className="card-img" src={`${config.backdropURL}${tvPayload[0].backdrop_path}`} alt="..."/>
    const jumboFourth = _.isEmpty(tvPayload) ? <ImageLoader /> : <img className="card-img" src={`${config.backdropURL}${tvPayload[1].backdrop_path}`} alt="..."/>
    
    return (
      <div>        
        <Row>
          <Col>
            <Carousel autoplay>
              <div>
                <div className="jumbotron-res">
                  { jumboSec }
                </div>
              </div>
              <div>
                <div className="jumbotron-res">
                  { jumboFirst }
                </div>
              </div>
              <div>
                <div className="jumbotron-res">
                  { jumboThird }
                </div>
              </div>
              <div>
                <div className="jumbotron-res">
                  { jumboFourth }
                </div>  
              </div>
            </Carousel>
          </Col>
        </Row>      
      </div>
    )
  }
}

const mapStateToProps = state => ({
    movieState: state.moviePopReducer,
    tvState: state.tvshowPopReducer
})

const mapDispatchToProps = dispatch => ({
    fetchMoviesPop: () => dispatch(ACTION_MOVIE.fetchMoviesPop()),
    fetchTvshowPop: () => dispatch(ACTION_TVSHOW.fetchTvshowPop())
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(JumbotronBoarding)) 