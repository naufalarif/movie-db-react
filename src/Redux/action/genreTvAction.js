import axios from "axios"
import config from "../../config"

export const GENRE_TV_REQUEST = 'GENRE_TV_REQUEST'
export const GENRE_TV_SUCCESS = 'GENRE_TV_SUCCESS'
export const GENRE_TV_FAILURE = 'GENRE_TV_FAILURE'

export const fetchGenreTv = () => {
  return dispatch => {
    dispatch(genreTvRequest)
    axios
      .get(`${config.baseURLGenre}tv/list?api_key=${config.apiKey}&language=en-US`)
      .then(res => dispatch(genreTvSuccess(res.data.genres)))
  }
}

export const genreTvRequest = () => ({
  type: GENRE_TV_REQUEST
})

export const genreTvSuccess = (data) => ({
  type: GENRE_TV_SUCCESS,
  payload: { data }
})

export const genreTvFailure = (err) => ({
  type: GENRE_TV_FAILURE,
  error: err
})