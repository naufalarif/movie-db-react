import config from '../../config'
import axios from 'axios'

/* ------------------ ACTION TYPE ------------------ */
export const MOVIE_POP_REQUEST = 'MOVIE_POP_REQUEST'
export const MOVIE_POP_SUCCESS = 'MOVIE_POP_SUCCESS'
export const MOVIE_POP_FAILURE = 'MOVIE_POP_FAILURE'


/* --------------- ACTION CREATORS -------------- */
export function fetchMoviesPop() {
  return dispatch => {
    dispatch(moviePopRequest())
    axios
      .get(`${config.baseURLMovie}movie/popular?api_key=${config.apiKey}&language=en-US&page=1`)
      .then(( data ) => {
        dispatch(moviePopSuccess(data))        
      })
      .catch((error) => {
        dispatch(moviePopFailure(error))
      })
  }
}

export const moviePopRequest = () => {
  return {
    type: MOVIE_POP_REQUEST
  }
}

export const moviePopSuccess = data => {
  return {
    type: MOVIE_POP_SUCCESS,
    payload: data
  }
}

export const moviePopFailure = error => {
  return {
    type: MOVIE_POP_FAILURE,
    payload: { error }
  }
}