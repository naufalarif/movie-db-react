import config from '../../config'
import axios from 'axios'

/* ------------------ ACTION TYPE ------------------ */
export const FETCH_MOVIE_DETAIL = 'FETCH_MOVIE_DETAIL'
export const GET_MOVIE_DETAIL_REQUEST = 'GET_MOVIE_DETAIL_REQUEST'
export const GET_MOVIE_DETAIL_SUCCESS = 'GET_MOVIE_DETAIL_SUCCESS'
export const GET_MOVIE_DETAIL_FAILURE = 'GET_MOVIE_DETAIL_FAILURE'


/* --------------- ACTION CREATORS -------------- */
export const fetchMovieDetail = idx => {
  return dispatch => {
    dispatch(getMovieDetailRequest())
    axios
      .get(`${config.baseURLMovie}movie/${idx}?api_key=${config.apiKey}&language=en-US&page=1`)
      .then(( data ) => {
        dispatch(getMovieDetailSuccess(data))
    })
      .catch((error) => {
        dispatch(getMovieDetailSuccess(error))
    })
    return {
      type: FETCH_MOVIE_DETAIL      
    }
  }
}

export const getMovieDetailRequest = () => {
  return {
    type: GET_MOVIE_DETAIL_REQUEST
  }
}

export const getMovieDetailSuccess = data => {
  return {
    type: GET_MOVIE_DETAIL_SUCCESS,
    payload: data
  }
}

export const getMovieDetailFailure = error => {
  return {
    type: GET_MOVIE_DETAIL_FAILURE,
    payload: { error }
  }
}