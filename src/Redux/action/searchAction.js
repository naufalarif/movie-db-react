import axios from "axios"
import config from "../../config"

export const GET_SEARCH_QUERY = 'GET_SEARCH_QUERY'
export const GET_SEARCH_REQUEST = 'GET_SEARCH_REQUEST'
export const GET_SEARCH_SUCCESS = 'GET_SEARCH_SUCCESS'
export const GET_SEARCH_FAILURE = 'GET_SEARCH_FAILURE'

export const getSearchQuery = query => {
  return dispatch => {
    dispatch(getSearchRequest())
    axios
      .get(`${config.baseURLSearch}movie?api_key=${config.apiKey}&query=${query}&page=1&include_adult=false`)
      .then((data) => {
        dispatch(getSearchSuccess(data))
      })
      .catch((error) => {
        dispatch(getSearchFailure(error))
      })
    return {
      type: GET_SEARCH_QUERY      
    }
  }
}

export const getSearchRequest = ()  => {
  return {
    type: GET_SEARCH_REQUEST    
  }
}


export const getSearchSuccess = data => {
  return {
    type: GET_SEARCH_SUCCESS,
    payload: data
  }
}

export const getSearchFailure = error => {
  return {
    type: GET_SEARCH_FAILURE,
    payload: { error }
  }
}