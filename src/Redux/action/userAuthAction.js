export const LOGIN_REQUEST = 'LOGIN_REQUEST'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGIN_FAILURE = 'LOGIN_FAILURE'

export const REGISTER_REQUEST = 'REGISTER_REQUEST'
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS'
export const REGISTER_FAILURE = 'REGISTER_FAILURE'

export const BLOCKER_EMPTY_FIELD = 'BLOCKER_EMPTY_FIELD'
export const BLOCKER_VALIDATED = 'BLOCKER_VALIDATED'
export const UNSUBS_BLOCKER = 'UNSUBS_BLOCKER'

/* ------------- LOGIN ------------ */
export const loginRequest = () => {
  return {
    type: LOGIN_REQUEST
  }
}

export const loginSuccess = () => {
  return {
    type: LOGIN_SUCCESS
  }
}

export const loginFailure = () => {
  return {
    type: LOGIN_FAILURE
  }
}

/* ------------- REGISTER ------------ */

export const registerRequest = () => {
  return {
    type: REGISTER_REQUEST
  }
}

export const registerSuccess = () => {
  return {
    type: REGISTER_SUCCESS
  }
}

export const registerFailure = () => {
  return {
    type: REGISTER_FAILURE
  }
}

export const blockerEmptyField = () => {
  return {
    type: BLOCKER_EMPTY_FIELD
  }
}

export const blockerValidated = () => {
  return {
    type: BLOCKER_VALIDATED
  }
}

export const unsubsBlocker = () => {
  return {
    type: UNSUBS_BLOCKER
  }
}