/* ------------- ACTION TYPE ------------ */
export const GET_TRENDING_REQUEST = 'GET_TRENDING_REQUEST'
export const GET_TRENDING_SUCCESS = 'GET_TRENDING_SUCCESS'
export const GET_TRENDING_FAILURE = 'GET_TRENDING_FAILURE'

/* -------------- ACTION CREATOR ------------ */
// export const fetchTrending = dispatch => {
//   return dispatch = () => {
//     axios
//       .get(`${config.baseURLMovie}trending/all/day?api_key=${config.apiKey}`)
//       .then((data) => {
//         dispatch(getTrendingSuccess(data))
//       })
//       .catch((err) => {
//         dispatch(getTrendingFailute(err))
//       })
//   }
// }

export const getTrendingRequest = () => {
  return {
    type: GET_TRENDING_REQUEST
  }
}

export const getTrendingSuccess = data => {
  return {
    type: GET_TRENDING_SUCCESS,
    payload: data
  }
}

export const getTrendingFailute = err => {
  return {
    type: GET_TRENDING_FAILURE,
    payload: err
  }
}