import config from '../../config'
import axios from 'axios'

/* ------------------ ACTION TYPE ------------------ */
export const FETCH_TVSHOW_DETAIL = 'FETCH_TVSHOW_DETAIL'
export const GET_TVSHOW_DETAIL_REQUEST = 'GET_MOVIE_TVSHOW_REQUEST'
export const GET_TVSHOW_DETAIL_SUCCESS = 'GET_MOVIE_TVSHOW_SUCCESS'
export const GET_TVSHOW_DETAIL_FAILURE = 'GET_MOVIE_TVSHOW_FAILURE'


/* --------------- ACTION CREATORS -------------- */
export const fetchTvshowDetail = idx => {
  return dispatch => {
    dispatch(getTvshowDetailRequest())
    axios
      .get(`${config.baseURLTvshow}tv/${idx}?api_key=${config.apiKey}&language=en-US&page=1`)
      .then(( data ) => {
        dispatch(getTvshowDetailSuccess(data))
    })
      .catch((error) => {
        dispatch(getTvshowDetailSuccess(error))
    })
    return {
      type: FETCH_TVSHOW_DETAIL      
    }
  }
}

export const getTvshowDetailRequest = () => {
  return {
    type: GET_TVSHOW_DETAIL_REQUEST
  }
}

export const getTvshowDetailSuccess = data => {
  return {
    type: GET_TVSHOW_DETAIL_SUCCESS,
    payload: data
  }
}

export const getTvshowDetailFailure = error => {
  return {
    type: GET_TVSHOW_DETAIL_FAILURE,
    payload: { error }
  }
}