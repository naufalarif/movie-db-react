import axios from "axios"
import config from "../../config"

export const GENRE_REQUEST = 'GENRE_REQUEST'
export const GENRE_SUCCESS = 'GENRE_SUCCESS'
export const GENRE_FAILURE = 'GENRE_FAILURE'

export const fetchGenre = () => {
  return dispatch => {
    dispatch(genreRequest)
    axios
      .get(`${config.baseURLGenre}movie/list?api_key=${config.apiKey}&language=en-US`)
      .then(res => dispatch(genreSuccess(res.data.genres)))
  }
}

export const genreRequest = () => ({
  type: GENRE_REQUEST
})

export const genreSuccess = (data) => ({
  type: GENRE_SUCCESS,
  payload: { data }
})

export const genreFailure = (err) => ({
  type: GENRE_FAILURE,
  error: err
})