import * as ACTIONS from '../action/genreTvAction'

const INITIAL_STATE = {
  data: {},
  fetching: false,
  success: false,
  errors: null
}

export default function genreTvReducer(state = INITIAL_STATE, action) {
  switch(action.type) {
    case ACTIONS.GENRE_TV_REQUEST:
      return {
        ...state,
        fetching: true,
        success: false,
        error: false
      }
    case ACTIONS.GENRE_TV_SUCCESS:
      return {
        ...state,
        fetching: false,
        data: action.payload.data,
        success: true,
        error: false
      }
    case ACTIONS.GENRE_TV_FAILURE:
      return {
        ...state,
        fetching: false,
        success: false,
        error: action.error
      }
    default: return state
  }
}