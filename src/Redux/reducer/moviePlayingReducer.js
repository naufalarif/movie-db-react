import * as ACTION_TYPE from '../action/moviePlayingAction'

const INITIAL_STATE = {
  items: [],
  fetching: false,
  success: false,
  error: null
}

export default function moviePlayingReducer(state = INITIAL_STATE, action) {
  switch(action.type) {
    case ACTION_TYPE.MOVIE_PLAYING_REQUEST:
      return {
        ...state,
        fetching: true,
        success: false,
        error: false
      }
    case ACTION_TYPE.MOVIE_PLAYING_SUCCESS:
      return {
        ...state,
        fetching: false,
        success: true,
        items: action.payload.data
      }
    case ACTION_TYPE.MOVIE_PLAYING_FAILURE:
      return{
        ...state,
        fetching: false,
        success: false,
        error: action.payload.error,
      }
    default: return state
  }
}