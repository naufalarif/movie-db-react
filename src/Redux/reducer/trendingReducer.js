import * as ACTION from '../action/trendingAction'

const INITIAL_STATE = {
  item: [],
  success: false,
  loading: false,
  error: null
}

export default function trendingReducer(state = INITIAL_STATE, action) {
  switch(action.type) {
    case ACTION.GET_TRENDING_REQUEST:
      return {
        ...state,
        success: false,
        loading: true,
        error: null
      }
    case ACTION.GET_TRENDING_SUCCESS:
      return {
        ...state,
        success: true,
        loading: false,
        item: action.payload.data
      }
    case ACTION.GET_TRENDING_FAILURE:
      return {
        ...state,
        success: false,
        loading: false,
        error: action.payload.err
      }
    default: 
      return {
        state
      }
  }
}