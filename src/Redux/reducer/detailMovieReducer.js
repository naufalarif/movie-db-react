import * as ACTION_TYPE from '../action/detailMovieAction'

const INITIAL_STATE = {  
  item: [],
  id: '',
  fetching: false,
  success: true,
  error: null
}

export default function detailMovieReducer(state = INITIAL_STATE, action) {
  switch(action.type) {
    case ACTION_TYPE.FETCH_MOVIE_DETAIL:
      return{
        ...state,
        fetching: true,
        id: action.payload.id
      }
    case ACTION_TYPE.GET_MOVIE_DETAIL_REQUEST:
      return {
        ...state,
        fetching: true,
        success: false,
        error: false
      }
    case ACTION_TYPE.GET_MOVIE_DETAIL_SUCCESS:
      return {
        ...state,
        fetching: false,
        success: true,
        item: action.payload.data
      }
    case ACTION_TYPE.GET_MOVIE_DETAIL_FAILURE:
      return {
        ...state,
        fetching: false,
        success: false,
        error: action.payload.error
      }
    default: return state
  }
}