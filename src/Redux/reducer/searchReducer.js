import * as ACTION_TYPE from '../action/searchAction'

const INITIAL_STATE = {
  items: [],
  query: '',
  fetching: false,  
  error: null
}

export default function searchReducer(state = INITIAL_STATE, action) {
  switch(action.type) {
    case ACTION_TYPE.GET_SEARCH_QUERY:
      return {
        ...state,
        query: action.payload.query
      }
    case ACTION_TYPE.GET_SEARCH_REQUEST:
      return {
        ...state,
        fetching: true,        
        error: null
      }
    case ACTION_TYPE.GET_SEARCH_SUCCESS:
      return {
        ...state,
        items: action.payload.data,
        fetching: false
      }
    case ACTION_TYPE.GET_SEARCH_FAILURE:
      return {
        ...state,
        fetching: false,
        error: action.payload.error,
        item: []
      }
    default:
      return state
  }
}