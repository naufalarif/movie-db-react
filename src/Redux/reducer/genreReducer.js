import * as ACTIONS from '../action/genreAction'

const INITIAL_STATE = {
  data: {},
  fetching: false,
  success: false,
  errors: null
}

export default function genreReducer(state = INITIAL_STATE, action) {
  switch(action.type) {
    case ACTIONS.GENRE_REQUEST:
      return {
        ...state,
        fetching: true,
        success: false,
        error: false
      }
    case ACTIONS.GENRE_SUCCESS:
      return {
        ...state,
        fetching: false,
        data: action.payload.data,
        success: true,
        error: false
      }
    case ACTIONS.GENRE_FAILURE:
      return {
        ...state,
        fetching: false,
        success: false,
        error: action.error
      }
    default: return state
  }
}