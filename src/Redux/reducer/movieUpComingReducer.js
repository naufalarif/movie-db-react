import * as ACTION_TYPE from '../action/movieUpComingAction'

const INITIAL_STATE = {
  items: [],
  fetching: false,
  success: false,
  error: null
}

export default function movieUpComingReducer(state = INITIAL_STATE, action) {
  switch(action.type) {
    case ACTION_TYPE.MOVIE_UPCOMING_REQUEST:
      return {
        ...state,
        fetching: true,
        success: false,
        error: false
      }
    case ACTION_TYPE.MOVIE_UPCOMING_SUCCESS:
      return {
        ...state,
        fetching: false,
        success: true,
        items: action.payload.data
      }
    case ACTION_TYPE.MOVIE_UPCOMING_FAILURE:
      return{
        ...state,
        fetching: false,
        success: false,
        error: action.payload.error,
      }
    default: return state
  }
}