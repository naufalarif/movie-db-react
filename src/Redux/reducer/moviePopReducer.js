import * as ACTION_TYPE from '../action/moviePopAction'

const INITIAL_STATE = {
  items: [],
  fetching: false,
  success: false,
  error: null
}

export default function moviePopReducer(state = INITIAL_STATE, action) {
  switch(action.type) {
    case ACTION_TYPE.MOVIE_POP_REQUEST:
      return {
        ...state,
        fetching: true,
        success: false,
        error: false
      }
    case ACTION_TYPE.MOVIE_POP_SUCCESS:
      return {
        ...state,
        fetching: false,
        success: true,
        items: action.payload.data
      }
    case ACTION_TYPE.MOVIE_POP_FAILURE:
      return{
        ...state,
        fetching: false,
        success: false,
        error: action.payload.error,
      }
    default: return state
  }
}