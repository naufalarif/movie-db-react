import * as ACTION_TYPE from '../action/userAuthAction'

/* ----- INITIAL STATE ----- */
const INITIAL_STATE = {
  data: {},
  fetching: false,
  success: false,  
  error: null,
  validated: false,
  emptyField: false
}

/* ------- REDUCER --------- */
const userAuthReducer = (state = INITIAL_STATE, action) => {
  switch(action.type) {

    // Login Reducer
    case ACTION_TYPE.LOGIN_REQUEST:
      return {
        ...state,
        success: false,
        fetching: true
      }
    case ACTION_TYPE.LOGIN_SUCCESS:
      return {
        ...state,
        fetching: false,
        success: true,
        error: false
      }
    case ACTION_TYPE.LOGIN_FAILURE:
      return {
        ...state,
        fetching: false,
        success: false,
        err: true
      }    

    // Register Reducer
    case ACTION_TYPE.REGISTER_REQUEST:
      return {
        ...state,
        fetching: true,
        success: false,
        error: null
      }
    case ACTION_TYPE.REGISTER_SUCCESS:
      return {
        ...state,
        fetching: false,
        success: true,
        error: false
      }
    case ACTION_TYPE.REGISTER_FAILURE:
      return {
        ...state,
        fetching: false,
        success: false,
        error: true
      }

    // Authtenticate Reducer
    case ACTION_TYPE.BLOCKER_VALIDATED:
      return {
        validated: true,
        emptyField: false
      }
    case ACTION_TYPE.BLOCKER_EMPTY_FIELD:
      return {
        emptyField: true,
        validated: false
      }
    case ACTION_TYPE.UNSUBS_BLOCKER:
      return {
        emptyField: false,
        validated: false
      }
    
    default:
      return state
  }
}

export default userAuthReducer