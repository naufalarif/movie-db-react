import { compose, applyMiddleware, createStore } from "redux"
import thunk from 'redux-thunk'
import createSagaMiddleware from 'redux-saga'
import rootReducer from "."
import rootSagas from "../../../Sagas"

// export default (rootReducer) => {
//   let enhancers = []
//   let middleware = []

//   typeof window === 'object' &&
//   const composeEnhancers =
//   window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
//     ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
//       // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
//     }) : compose

//   // add our normal middleware to the list
//   enhancers.push(applyMiddleware(...middleware))

//   // a function which can create our store and auto-persist the data
//   const store = createStore(rootReducer, applyMiddleware(thunk), composeEnhancers(...enhancers))

//   return { store }
// }

const configureStore = () => {
  const sagaMiddleware = createSagaMiddleware()
  const store = createStore(
    rootReducer,
    compose(
      applyMiddleware(sagaMiddleware, thunk),
      window.__REDUX_DEVTOOLs_EXTENSION__ &&
        window.__REDUX_DEVTOOLs_EXTENSION__(),
    )
  )
  sagaMiddleware.run(rootSagas)

  // store.dispatch({ type: })
  return store
}

export default configureStore