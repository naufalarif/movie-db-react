import UserAuthReducer from '../userAuthReducer'
import MoviePopReducer from '../moviePopReducer'
import MoviePlayingReducer from '../moviePlayingReducer'
import MovieUpComingReducer from '../movieUpComingReducer'
import TvshowPopReducer from '../tvshowPopReducer'
import TvshowOnAirReducer from '../tvshowOnAirReducer'
import TvshowLatestReducer from '../tvshowLatestReducer'
import SearchReducer from '../searchReducer'
import DetailMovieReducer from '../detailMovieReducer'
import DetailTvshowReducer from '../detailTvshowReducer'
import GenreReducer from '../genreReducer'
import GenreTvReducer from '../genreTvReducer'

import { combineReducers } from 'redux'

const rootReducer = combineReducers({
  userAuthReducer: UserAuthReducer,
  moviePopReducer: MoviePopReducer,
  moviePlayingReducer: MoviePlayingReducer,
  movieUpComingReducer: MovieUpComingReducer,
  tvshowPopReducer: TvshowPopReducer,
  tvshowOnAirReducer: TvshowOnAirReducer,
  tvshowLatestReducer: TvshowLatestReducer,
  searchReducer: SearchReducer,
  detailMovieReducer: DetailMovieReducer,
  detailTvshowReducer: DetailTvshowReducer,
  genreReducer: GenreReducer,
  genreTvReducer: GenreTvReducer
})

export default rootReducer