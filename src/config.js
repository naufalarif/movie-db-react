export default {
  // User
  baseURLUser: 'https://reqres.in/',
  regsiterURL: '/api/register',
  loginURL: '/api/login',
  
  // API Key
  apiKey: 'b91968a52f320dcc633ccaa6afd28642',  
  // Base URL
  baseURLMovie: 'https://api.themoviedb.org/3/',
  baseURLSearch: 'https://api.themoviedb.org/3/search/',
  baseURLTvshow: 'https://api.themoviedb.org/3/',
  baseURLGenre: 'https://api.themoviedb.org/3/genre/',
  utubeURL: 'https://www.youtube.com/results?search_query=',

  // Image URL
  backdropURL: 'https://image.tmdb.org/t/p/original/',
  posterURL: 'https://image.tmdb.org/t/p/original/',
}