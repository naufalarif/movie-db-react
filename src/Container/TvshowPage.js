import React, { Component } from 'react'
import { withRouter } from 'react-router'
import _ from 'lodash'

// Redux
import { connect } from 'react-redux'
import * as ACTION_POP from '../Redux/action/tvshowPopAction'
import * as ACTION_PLAYING from '../Redux/action/tvshowOnAirAction'
import * as ACTION_UPCOMING from '../Redux/action/tvshowLatestAction'
import * as ACTION_GENRE from '../Redux/action/genreTvAction'

// Antd
import { Row, Col } from 'antd'

// Comp
import NavigationBar from '../Components/navigation/NavigationBar'
import TvList from '../Components/list/slicker/TvList'
import Footer from '../Components/Footer'
import CategoryTvList from '../Components/list/slicker/CategoryTvList'
import MyTvList from '../Components/list/slicker/MyTvList'

class TvshowPage extends Component {
  constructor(props) {
    super(props)
      this.state = {
        genre: 10759 // set default genre as action
      }
  }

  componentDidMount() {
    this.props.fetchTvshowPop()
    this.props.fetchTvshowOnAir()
    this.props.fetchTvshowLatest()  
    this.props.fetchGenre()
  }

  handleGenreClick = (id) => {
    this.setState({ genre: id })
  }

  handleMoreClick = (path) => {
    this.props.history.push(`/tvshow/${path}`)
  }

  _renderMylist = () => {
    const tvList = JSON.parse(localStorage.getItem('tvlist'))
    
    return (
      <div className='pl-4 pr-4 pb-4'>
        <div>
          <h4>My List</h4>
        </div>
        <div>
          <MyTvList payload={tvList} />
        </div>
      </div>
    )
  }

  render() {    
    // set data here
    const { tvshowOnAirState, tvshowPopState, genreState } = this.props    
    const payloadPop = !_.isEmpty(tvshowPopState.items) ? tvshowPopState.items.results : []
    const payloadOnAir = !_.isEmpty(tvshowOnAirState.items) ? tvshowOnAirState.items.results : []
    const genrePayload = !_.isEmpty(genreState.data) ? genreState.data : []
    const genreArr = []
    const arr = []

    // logic here
    payloadPop.forEach(items => {
      const genre = items.genre_ids
      if(genre.includes(this.state.genre)) {
        arr.push(items)
      }
    })

    payloadOnAir.forEach(items => {
      const genre = items.genre_ids
      if(genre.includes(this.state.genre)) {
        arr.push(items)
      }
    })

    genrePayload.forEach(items => {
      if(_.isEqual(items.name, 'Action & Adventure') || _.isEqual(items.name, 'Comedy') || _.isEqual(items.name, 'Drama') || _.isEqual(items.name, 'Sci-Fi & Fantasy') ) {
        genreArr.push(items)
      }
    })

    const mylist = !_.isEmpty(localStorage.getItem('tvlist')) ? this._renderMylist() : null

    // logic not allowed in return!
    return(
      <div className='movie-container'>
        <NavigationBar tvshow="active"/>
        
        <div className="pl-4 pr-4 pt-5 pb-4">
          <div className="header-title">
            <Row justify='space-between'>
              <Col xs={16} sm={14} md={20} lg={16} xl={20}>
                <div>
                  <h4>On Air Show</h4>
                </div>
              </Col>
              <Col xs={8} sm={4} md={4} lg={4} xl={4}>
                <div className="text-link">
                  <span onClick={() => this.handleMoreClick('onair')}>Show More</span>
                </div>
              </Col>
            </Row>
          </div>
          <div className='pl-4 pr-4 pb-4'>
            <TvList payload={payloadOnAir} />
          </div>
        </div>

        <div className='pl-4 pr-4'>
          <div className='pb-1'>
            <h4>Browse by category</h4>
          </div>
          <div className='pb-4'>
            <Row>
              {
                genreArr.map((data, idx) => {
                  return (
                    <Col xs={10} sm={10} md={4} lg={4} xl={2} 
                      key={idx} span={3} onClick={() => this.handleGenreClick(data.id)}>
                      <div className='category-container'>
                        <span>{data.name.substring(0, 7)}</span>
                      </div>
                    </Col>
                  )
                })
              }
            </Row>
          </div>
          <div className='pl-4 pr-4 pt-1 pb-3'>
            <CategoryTvList payload={arr} />
          </div>
        </div>

        <div className="pl-4 pr-4 pb-4">
          <div className="header-title">
            <Row>
              <Col xs={16} sm={14} md={20} lg={16} xl={20}>
                <div>
                  <h4>Popular Tv Show</h4>
                </div>
              </Col>
              <Col xs={8} sm={4} md={4} lg={4} xl={4}>
                <div className="text-link">
                  <span onClick={() => this.handleMoreClick('popular')}>Show More</span>
                </div>
              </Col>
            </Row>
          </div>
          <div className='pl-4 pr-4 pb-4 pt-1'>
            <TvList payload={payloadPop} />
          </div>
        </div>         
        
        <div>
          {mylist}
        </div>
        <Footer />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  tvshowPopState: state.tvshowPopReducer,
  tvshowOnAirState: state.tvshowOnAirReducer,
  genreState: state.genreTvReducer
})

const mapDispatchToProps = dispatch => ({
  fetchTvshowPop: ()=> dispatch(ACTION_POP.fetchTvshowPop()),
  fetchTvshowOnAir: ()=> dispatch(ACTION_PLAYING.fetchTvshowOnAir()),
  fetchTvshowLatest: ()=> dispatch(ACTION_UPCOMING.fetchTvshowLatest()),
  fetchGenre: () => dispatch(ACTION_GENRE.fetchGenreTv())
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(TvshowPage))