import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import _ from 'lodash'

// Redux
import { connect } from 'react-redux'
import * as ACTION_MOVIE_POP from '../Redux/action/moviePopAction'
// import * as ACTION_MOVIE_PLAYING from '../Redux/action/moviePlayingAction'
import * as ACTION_TV_POP from '../Redux/action/tvshowPopAction'
// import * as ACTION_TV_AIR from '../Redux/action/tvshowOnAirAction'

// Antd
import { Row, Col } from 'antd'

// Comp
import NavigationBar from '../Components/navigation/NavigationBar'
import MovieList from '../Components/list/slicker/MovieList'
import Footer from '../Components/Footer'
import TvList from '../Components/list/slicker/TvList'
import MyMovieList from '../Components/list/slicker/MyMovieList'
import MyTvList from '../Components/list/slicker/MyTvList'

class HomePage extends Component {

  componentDidMount() {
    this.props.fetchMoviesPop()
    this.props.fetchTvPop()
  }

  handleMoreMovieClick = (path) => {
    this.props.history.push(`movie/${path}`)
  }

  handleMoreTvClick = (path) => {
    this.props.history.push(`tvshow/${path}`)
  }

  _renderMyMovielist = () => {
    const movieList = JSON.parse(localStorage.getItem('movielist'))
    
    return (
      <div className='pl-4 pr-4 pb-4'>
        <div>
          <h4>My Movie List</h4>
        </div>
        <div>
          <MyMovieList payload={movieList} />
        </div>
      </div>
    )
  }

  _renderMyTvlist = () => {
    const tvList = JSON.parse(localStorage.getItem('tvlist'))
    
    return (
      <div className='pl-4 pr-4 pb-4'>
        <div>
          <h4>My Show List</h4>
        </div>
        <div>
          <MyTvList payload={tvList} />
        </div>
      </div>
    )
  }

  _renderMylist = () => {
    const data = []
    const movieList = JSON.parse(localStorage.getItem('movielist'))
    const tvList = JSON.parse(localStorage.getItem('tvlist'))

    const movie = Object.assign({}, movieList)
    const tv = Object.assign({}, tvList)
    data.push(tv)
    data.push(movie)

    return (
      <div className='pl-4 pr-4 pb-4'>
        <div>
          <h4>My List</h4>
        </div>
        <div>
          <span>Movie</span>
        </div>
        <div>
          <MyMovieList payload={movieList} />
        </div>
        <div>
          <span>Tv Show</span>
        </div>
        <div>
          <MyTvList payload={tvList} />
        </div>
      </div>
    )
  }

  render() {
    // set data here
    const { moviePopState, tvPopState } = this.props
    const payloadPop = !_.isEmpty(moviePopState.items) ? moviePopState.items.results : []
    const payloadTvPop = !_.isEmpty(tvPopState.items) ? tvPopState.items.results : [] 

    // logic here
    const payloadMylist = 
      localStorage.getItem('movielist') && localStorage.getItem('tvlist') ? this._renderMylist()
        : !localStorage.getItem('movielist') && localStorage.getItem('tvlist') ? this._renderMyTvlist()
        : !localStorage.getItem('tvlist') && localStorage.getItem('movielist') ? this._renderMyMovielist()
        : null
    
    // logic not allowed in returd!
    return(
      <div>
        <NavigationBar home="active" color="#fff" />
        <div className='movie-container pt-5'>
          <div className="pl-4 pr-4 pb-4">
            <div className="header-title">
              <Row>
                <Col xs={16} sm={14} md={20} lg={16} xl={20}>
                  <div>
                    <h4>Popular Movie</h4>
                  </div>
                </Col>
                <Col xs={8} sm={4} md={4} lg={4} xl={4}>
                  <div className="text-link">
                    <span onClick={() => this.handleMoreMovieClick('popular')}>Show More</span>
                  </div>
                </Col>
              </Row>
            </div>
            <div className='pl-4 pr-4 pb-4 pt-1'>
              <MovieList payload={payloadPop} />
            </div>
          </div>

          <div>
            {payloadMylist}
          </div>

          <div className="pl-4 pr-4 pb-4">
            <div className="header-title">
              <Row>
                <Col xs={16} sm={14} md={20} lg={16} xl={20}>
                  <div>
                    <h4>Popular Tv Show</h4>
                  </div>
                </Col>
                <Col xs={8} sm={4} md={4} lg={4} xl={4}>
                  <div className="text-link">
                    <span onClick={() => this.handleMoreTvClick('popular')}>Show More</span>
                  </div>
                </Col>
              </Row>
            </div>
            <div className='pl-4 pr-4 pb-4 pt-1'>
              <TvList payload={payloadTvPop} />
            </div>
          </div>
        </div>
        <Footer />
      </div>
    )     
  }
}

const mapStateToProps = state => ({
  moviePopState: state.moviePopReducer,
  tvPopState: state.tvshowPopReducer
})

const mapDispatchToProps = dispatch => ({
  fetchMoviesPop: () => dispatch(ACTION_MOVIE_POP.fetchMoviesPop()),
  fetchTvPop: () => dispatch(ACTION_TV_POP.fetchTvshowPop()),
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(HomePage))