import React, { Component } from 'react'
import { withRouter } from 'react-router'
import config from '../../config'
import { create } from 'apisauce'
import _ from 'lodash'

// redux setup
import { connect } from 'react-redux'
import * as ACTIONS from '../../Redux/action/userAuthAction'

// style
import { Form, Input, Button, Checkbox } from 'antd'

// Comp
import LoadingSpin from '../../Components/loader/LoadingSpin'

const baseURL = config.baseURLUser

const api = create({
  baseURL: baseURL
})

class RegisterPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: ''
    }
  }

  componentWillUnmount() {
    this.props.unsubsBlocker()
  }

  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
  }

  handleSubmit = async() => { 
    const { email, password } = this.state
    const { history, blockerEmptyField, registerFailure, registerSuccess, registerRequest } = this.props
    let data = {
      email: email,
      password: password
    }

    if(!_.isEmpty(email, password)) {        
      registerRequest()
      try {        
        let res = await api.post('/api/register', data)
        let dataToken = res.data.token
        if (dataToken !== undefined) {
          registerSuccess()
          history.push('/login')
        } else {
          registerFailure()
        }        
      } catch {
        registerFailure()
      }
    } else {
      blockerEmptyField()
    }
  }

  render() {    
    const { authState, history } = this.props
    const textEmpty = authState.emptyField ? <span style={{ color:'rgb(240, 85, 80)' }}>Please fill the field!</span> : null
    const button = authState.loading ? <LoadingSpin /> : <Button onClick={this.handleSubmit} style={{marginTop: '40px'}}>Sign Up</Button>

    // eve.holt@reqres.in
    // cityslicka
    return (
      <div className="container register">
        <Form
          name="normal_login"
          className="form"
          initialValues={{ remember: true }}
        >
          <h3>Register</h3>
          <Form.Item
            name="username"
            rules={[{ required: true, message: 'Please input your Username!' }]}
          >
            <Input 
              id='email'
              type="text" 
              placeholder="Email" 
              onChange={this.handleChange}
            /> 
          </Form.Item>

          <Form.Item
            name="password"
            rules={[{ required: true, message: 'Please input your Password!' }]}
          >
            <Input        
              id='password'          
              type="password"
              placeholder="Password"
              onChange={this.handleChange}
            />
          </Form.Item>
            
          <div>                 
            {textEmpty}
          </div>
            
          <Form.Item>                                      
            <Form.Item name="remember" valuePropName="checked" noStyle>
              <div>
                <Checkbox><span>Agree with terms</span></Checkbox>  
              </div>                                                            
            </Form.Item>                                                                       
          </Form.Item>

          <Form.Item>
            {button}
            <div className="text-action">
              <p>Have an account? <span onClick={() => history.push('/login')}>Login now</span></p>
            </div>  
          </Form.Item>                                                                     
        </Form>      
      </div>
    )
  }
}

const mapStateToProps = state => ({
    authState: state.userAuthReducer
})

const mapDispatchToProps = dispatch => ({
    registerRequest: () => dispatch(ACTIONS.registerRequest()),
    registerSuccess: () => dispatch(ACTIONS.registerSuccess()),
    registerFailure: () => dispatch(ACTIONS.registerFailure()),
    blockerEmptyField: () => dispatch(ACTIONS.blockerEmptyField()),
    unsubsBlocker: () => dispatch(ACTIONS.unsubsBlocker())
})

export default withRouter(connect( mapStateToProps, mapDispatchToProps) (RegisterPage))