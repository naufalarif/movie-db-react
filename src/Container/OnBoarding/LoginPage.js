import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { create } from 'apisauce'
import _ from 'lodash'
import config from '../../config'

// Style & Antd
import { Form, Input, Button, Checkbox } from 'antd'

// Comp
import LoadingSpin from '../../Components/loader/LoadingSpin'

// redux setup
import * as ACTIONS from '../../Redux/action/userAuthAction'
import { connect } from 'react-redux'

const baseURL = config.baseURLUser

const api = create({
  baseURL: baseURL
})

class LoginPage extends Component {  
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: ''
    }
  }

  componentWillUnmount() {
    this.props.unsubsBlocker()
  }

  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
  }

  handleSubmit = async() => {
    const { history, 
      blockerValidated, 
      blockerEmptyField, 
      loginSuccess, 
      loginFailure, 
      loginRequest } = this.props

    const { email, password, token } = this.state

    let data = {
      email: email,
      password: password,
      token: ''
    }

    if(!_.isEmpty(this.state.email, this.state.password)) {     
      loginRequest()
        try {
          let res = await api.post('/api/login', data)          
          let dataToken = await res.data.token
          this.setState({ token: dataToken })
          if(_.isEmpty(dataToken)) {
            localStorage.setItem('token', token)            
            history.push('/')
          } else {            
            blockerValidated()
          }
        loginSuccess()
        } catch {
          loginFailure()
        }    
    } else {
      blockerEmptyField()
    }
  }

  // pistol
  // cityslicka
  render() {
    const { email, password } = this.state
    const { authState, history } = this.props    

    const validatedText = authState.validated ? <span style={{color:'rgb(240, 85, 80)'}}>Username or password incorrect!</span> : null
    const emptyFieldText = authState.emptyField ? <span style={{color:'rgb(240, 85, 80)'}}>Please fill the field!</span> : null
    const button = authState.fetching ? <LoadingSpin /> : <Button onClick={this.handleSubmit}>Log in</Button>

    return (
      <div className="container login">
        <Form
          name="normal_login"
          className="form"
          initialValues={{ remember: true }}          
        >
          <h3>Sign In</h3>
          <Form.Item
            name="username"
            rules={[{ required: true, message: 'Please input your Username!' }]}
          >
            <Input 
              type="text" 
              placeholder="Username" 
              value={email} 
              onChange={(e)=> this.setState({ email: e.target.value })}
            /> 
          </Form.Item>

          <Form.Item
            name="password"
            rules={[{ required: true, message: 'Please input your Password!' }]}
          >
            <Input                  
              type="password"
              placeholder="Password"
              value={password}
              onChange={(e)=> this.setState({ password: e.target.value })}
            />
          </Form.Item>
            <div>
              {validatedText}
              {emptyFieldText}
            </div>
          <Form.Item>                                      
          <Form.Item name="remember" valuePropName="checked" noStyle>
            <div>
              <Checkbox><span>Remember me</span></Checkbox>  
            </div>                                                            
          </Form.Item>                                                                       
        </Form.Item>
        <Form.Item>
          <div>
            <span className="login-form-forgot">
              Forgot password
            </span> 
          </div>
            {button}
              <div className="text-action">
                <p>New to Netflix? <span onClick={() => history.push('/register')}>Sign up now</span></p>
              </div>  
            </Form.Item>                                                                     
          </Form>                          
      </div>
    )
  }
}

const mapStateToProps = state => ({
    authState: state.userAuthReducer
})

const mapDispatchToProps = dispatch => ({
    loginRequest: () => dispatch(ACTIONS.loginRequest()),
    loginSuccess: () => dispatch(ACTIONS.loginSuccess()),
    loginFailure: () => dispatch(ACTIONS.loginFailure()),    
    blockerValidated: () => dispatch(ACTIONS.blockerValidated()),
    blockerEmptyField: () => dispatch(ACTIONS.blockerEmptyField()),
    unsubsBlocker: () => dispatch(ACTIONS.unsubsBlocker())
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps) (LoginPage))