import React, { Component } from 'react'
import { withRouter } from 'react-router'

class PageNotFound extends Component {
  render() {
    return (
      <div>
        Not Found
      </div>
    )
  }
}

export default withRouter(PageNotFound)
