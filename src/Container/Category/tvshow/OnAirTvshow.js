import React, { Component } from 'react'
import { withRouter } from 'react-router'
import _ from 'lodash'

// Redux
import { connect } from 'react-redux'
import * as ACTION_TV from '../../../Redux/action/tvshowOnAirAction'
import * as ACTION_GENRE from '../../../Redux/action/genreTvAction'

// Comp
import TvMoreList from '../../../Components/list/TvMoreList'
import NavigationBar from '../../../Components/navigation/NavigationBar'
import LoadingSpin from '../../../Components/loader/LoadingSpin'
import CategoryTvList from '../../../Components/list/slicker/CategoryTvList'

// Antd
import { BackTop, Row, Col } from 'antd'

class OnAirtTvshow extends Component {
  constructor(props) {
    super(props)
    this.state = {
      genre: 10759 // set default genre as action
    }
  }

  componentDidMount() {
    this.props.fetchTvshowOnAir()
    this.props.fetchGenre()
  }

  handleGenreClick = (id) => {
    this.setState({ genre: id })
  }

  render() {
    // set data here
    const { tvshowState, genreState } = this.props
    const payload = !_.isEmpty(tvshowState.items) ? tvshowState.items.results : []
    const genrePayload = !_.isEmpty(genreState.data) ? genreState.data : []
    const genreArr = []
    const arr = []

    // logic here
    payload.forEach(items => {
      const genre = items.genre_ids
      if(genre.includes(this.state.genre)) {
        arr.push(items)
      }
    })

    genrePayload.forEach(items => {
      if(_.isEqual(items.name, 'Action & Adventure') || _.isEqual(items.name, 'Comedy') || _.isEqual(items.name, 'Drama') || _.isEqual(items.name, 'Sci-Fi & Fantasy') ) {
        genreArr.push(items)
      }
    })

    // logic not allowed in return!
    if(tvshowState.fetching) {
      return <LoadingSpin />
    } else {
      return(
        <div className='movie-container'>
          <NavigationBar tvshow="active"/>
          <BackTop>
            <div className="ant-back-top-inner"><b>UP</b></div>
          </BackTop>
          
          <div className="pl-4 pr-4 pt-4 pb-4 category">
            <div>
              <div className='pb-1'>
                <h4>Browse by category</h4>
              </div>
              <div className='pb-4'>
                <Row>
                  {
                    genreArr.map((data, idx) => {
                      return (
                        <Col xs={10} sm={10} md={4} lg={4} xl={2} 
                          key={idx} span={3} onClick={() => this.handleGenreClick(data.id)}>
                          <div className='category-container'>
                            <span>{data.name.substring(0, 7)}</span>
                          </div>
                        </Col>
                      )
                    })
                  }
                </Row>
              </div>
              <div className='pl-4 pr-4 pt-1 pb-3'>
                <CategoryTvList payload={arr} />
              </div>
            </div>

            <h4>On Air Show</h4>
            <TvMoreList payload={payload} />
          </div>
        </div>
      )
    }
  }
}

const mapStateToProps = state => ({
    tvshowState: state.tvshowOnAirReducer,
    genreState: state.genreTvReducer
})

const mapDispatchToProps = dispatch => ({
    fetchTvshowOnAir: () => dispatch(ACTION_TV.fetchTvshowOnAir()),
    fetchGenre: () => dispatch(ACTION_GENRE.fetchGenreTv())
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(OnAirtTvshow))