import React, { Component } from 'react'
import { withRouter } from 'react-router'
import _ from 'lodash'

// Redux
import { connect } from 'react-redux'
import * as ACTION from '../../../Redux/action/tvshowPopAction'

// Comp
import CardTvshow from '../../../Components/card/CardTvshow'
import NavigationBar from '../../../Components/navigation/NavigationBar'
import LoadingSpin from '../../../Components/loader/LoadingSpin'

// Antd
import { BackTop } from 'antd'

class PopTvshow extends Component {

  componentDidMount() {
    this.props.fetchTvshowPop()
  }

  render() {
    const { tvshowState } = this.props
    const payload = !_.isEmpty(tvshowState.items) ? tvshowState.items.results : []

    if(tvshowState.fetching) {
      return <LoadingSpin />
    } else {
      return(
        <div>
          <NavigationBar />     
          <BackTop>
            <div className="ant-back-top-inner"><b>UP</b></div>
          </BackTop>       
          <div className="container category" style={{marginTop: '30px'}}>
            <h4>Popular on Netflix</h4>
            <CardTvshow results={payload} />
            <div className="divided"></div>
          </div>
        </div>
      )
    }
  }
}

const mapStateToProps = state => ({
    tvshowState: state.tvshowOnAirReducer
})

const mapDispatchToProps = dispatch => ({
    fetchTvshowPop: () => dispatch(ACTION.fetchTvshowPop())
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PopTvshow))