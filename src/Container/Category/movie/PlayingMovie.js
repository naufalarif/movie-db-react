import React, { Component } from 'react'
import { withRouter } from 'react-router'
import _ from 'lodash'

// Redux
import { connect } from 'react-redux'
import * as ACTION_MOVIE from '../../../Redux/action/moviePlayingAction'
import * as ACTION_GENRE from '../../../Redux/action/genreAction'

// Comp
import NavigationBar from '../../../Components/navigation/NavigationBar'
import LoadingSpin from '../../../Components/loader/LoadingSpin'
import Footer from '../../../Components/Footer'
import CategoryList from '../../../Components/list/slicker/CategoryList'
import MovieMoreList from '../../../Components/list/MovieMoreList'

// Antd
import { BackTop, Row, Col } from 'antd'

class PlayingMovie extends Component {
  constructor(props) {
    super(props)
    this.state = {
      genre: 28 // set default genre as action
    }
  }

  componentDidMount() {
    this.props.fetchMoviesPlaying()
    this.props.fetchGenre()
  }

  handleGenreClick = (id) => {
    this.setState({ genre: id })
  }

  render() {
    // set data here
    const { moviePlayingState, genreState } = this.props
    const payload = !_.isEmpty(moviePlayingState.items) ? moviePlayingState.items.results : []
    const genrePayload = !_.isEmpty(genreState.data) ? genreState.data : []
    const genreArr = []
    const arr = []

    // logic here
    payload.forEach(items => {
      const genre = items.genre_ids
      if(genre.includes(this.state.genre)) {
        arr.push(items)
      }
    })

    genrePayload.forEach(items => {
      if(_.isEqual(items.name, 'Action') || _.isEqual(items.name, 'Adventure') || _.isEqual(items.name, 'Fantasy') || _.isEqual(items.name, 'Drama') || _.isEqual(items.name, 'Horror') ) {
        genreArr.push(items)
      }
    })
    
    // logic not allowed in return!
    if(moviePlayingState.fetching) {
      return <LoadingSpin />
    } else {
      return(
        <div>
          <NavigationBar movie="active"/> 
          <BackTop>
            <div className="ant-back-top-inner"><b>UP</b></div>
          </BackTop>
          <div className="pl-4 pr-4 pt-4 pb-4 category">
            <div>
              <div className='pb-1'>
                <h4>Browse by category</h4>
              </div>
              <div className='pb-4'>
                <Row>
                  {
                    genreArr.map((data, idx) => {
                      return (
                        <Col xs={10} sm={10} md={4} lg={4} xl={2} 
                          key={idx} span={3} onClick={() => this.handleGenreClick(data.id)}>
                          <div className='category-container'>
                            <span>{data.name}</span>
                          </div>
                        </Col>
                      )
                    })
                  }
                </Row>
              </div>
              <div className='pl-4 pr-4 pt-1 pb-3'>
                <CategoryList payload={arr} />
              </div>
            </div>

            <h4>Now Playing</h4>
            <MovieMoreList payload={payload} />
          </div>

          <Footer />
        </div>      
      )
    }
  }
}

const mapStateToProps = state => ({
  moviePlayingState: state.moviePlayingReducer,
  genreState: state.genreReducer
})

const mapDispatchToProps = dispatch => ({
  fetchMoviesPlaying: () => dispatch(ACTION_MOVIE.fetchMoviesPlaying()),
  fetchGenre: () => dispatch(ACTION_GENRE.fetchGenre())
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PlayingMovie))