import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import config from '../../config'
import _ from 'lodash'

// redux setup
import * as ACTIONS from '../../Redux/action/detailMovieAction'
import { connect } from 'react-redux'

// Icon
// import Star from '../../static/assets/star.svg'
// import Play from '../../static/assets/play-button.svg'
// import Bg from '../../static/assets/bg.jpg'

// Comp
import NavigationBar from '../../Components/navigation/NavigationBar'
// import ImageLoader from '../../Components/loader/ImageLoader'

// Antd
import { Row, Col, Button } from 'antd'

const ButtonAddList = ({ onClick }) => {
  return (
    <div className='btn-list'>
      <Button onClick={onClick}>
        ADD TO MY LIST
      </Button>
    </div>
  )
}

const ButtonRemove = ({ onClick }) => {
  return (
    <div className='btn-list'>
      <Button onClick={onClick}>
        REMOVE FROM MY LIST
      </Button>
    </div>
  )
}


class MovieDetailPage extends Component {

  componentDidMount() {
    const { payload } = this.props.location.state
    this.props.fetchMovieDetail(payload)
  }

  handleWatchTrailer = (url) => {
    window.location.href = `${config.utubeURL}${url}`
  }

  handleAddList = (data) => {
    const arrData = []
    arrData.push(data)
    if(_.isEmpty(localStorage.getItem('movielist'))) {
      localStorage.setItem('movielist', JSON.stringify(arrData))
      this.setState({ added: true })
    } else {
      const store = JSON.parse(localStorage.getItem('movielist'))
      store.push(data)
      localStorage.setItem('movielist', JSON.stringify(store))
      this.setState({ added: true })
    }
  }

  handleRemoveList = (payload) => {
    const data = JSON.parse(localStorage.getItem('movielist'))
    if(data.length > 1) {
      data.forEach((item, idx) => {
        if(item.title === payload.title) {
          const index = idx
          data.splice(index, index)
          localStorage.setItem('movielist', JSON.stringify(data))
          this.setState({ added: false })
        }
      })
    } else {
      localStorage.removeItem('movielist')
      this.setState({ added: false })
    }
  }
    
  render() {
    // set data here
    const { movieState } = this.props
    const payload = !_.isEmpty(movieState.item) ? movieState.item : {}
    const genrePayload = !_.isEmpty(payload) ? payload.genres.map(genres => genres.name) : []
    const datePayload = !_.isEmpty(payload) ? payload.release_date : {}
    const titlePayload = !_.isEmpty(payload) ? payload.title : {}

    // Logic here
    const genre = genrePayload.toString().replace(/,/g, "  |   ")
    const releaseDate = !_.isEmpty(datePayload) ? datePayload.substring(0,4) : ''
    const title = !_.isEmpty(titlePayload) ? titlePayload.toUpperCase() : ''
    const store = localStorage.getItem('movielist') ? JSON.parse(localStorage.getItem('movielist')) : []
    const added = []
    if(!_.isEmpty(store)) {
      store.forEach(data => {
        if(data.title === payload.title) {
          added.push(data.title)
        }
      })
    }
    const button = !_.isEmpty(added) ? <ButtonRemove onClick={() => this.handleRemoveList(payload)}/> : <ButtonAddList onClick={() => this.handleAddList(payload)} />

    // logic now allowed in return!
    return (
      <div className='detail-container'>
        <NavigationBar movie="active" />
        <div>
          <img 
            className='bg-detail'
            src={`${config.backdropURL}${payload.backdrop_path}`} 
            width='100%'
            alt='' 
          />
        </div>
        <div className='card-img-overlay'>
          <div className='content-detail-container'>
            <Row>
              <Col xs={24} sm={12} md={8} lg={8} xl={8} className='text-center'>
                <div className='pl-2 pr-2 pb-4'>
                  <img 
                    className='poster-detail'
                    src={`${config.posterURL}${payload.poster_path}`}
                    alt=''
                    width='80%'
                  />
                </div>
              </Col>
              <Col xs={24} sm={12} md={10} lg={10} xl={10}>
                <div>
                  <div>
                    <p>M O V I E | {releaseDate.toString()}</p>
                  </div>
                  <div className='pb-5'>
                    <h1>{title}</h1>
                  </div>
                  <div>
                    <p><b>{genre}</b></p>
                  </div>
                  <div className='pb-5'>
                    <p>{payload.overview}</p>
                  </div>
                  <Row>
                    <Col xs={24} sm={24} md={24} lg={12} xl={12} className='pb-3'>
                      <div className='btn-trailer'>
                        <Button onClick={() => this.handleWatchTrailer(payload.title)}>
                          WATCH TRAILER
                        </Button>
                      </div>
                    </Col>
                    <Col xs={24} sm={24} md={24} lg={12} xl={12} className='pb-2'>
                      {button}
                    </Col>
                  </Row>
                </div>
              </Col>
            </Row>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
    movieState: state.detailMovieReducer
})

const mapDispatchToProps = dispatch => ({
    fetchMovieDetail: (idx) => dispatch(ACTIONS.fetchMovieDetail(idx))
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MovieDetailPage))