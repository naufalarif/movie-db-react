import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import _ from 'lodash'
import config from '../../config'

// redux setup
import * as ACTIONS from '../../Redux/action/detailTvshowAction'
import { connect } from 'react-redux'

// Icon & Img
// import Star from '../../static/assets/star.svg'
// import Play from '../../static/assets/play-button.svg'
// import ImageLoader from '../../Components/loader/ImageLoader'

// Antd
import { Row, Col, Button } from 'antd'

// Comp
import NavigationBar from '../../Components/navigation/NavigationBar'
import EpisodeList from '../../Components/list/slicker/EpisodeList'
import Footer from '../../Components/Footer'

const ButtonAddList = ({ onClick }) => {
  return (
    <div className='btn-list'>
      <Button onClick={onClick}>
        ADD TO MY LIST
      </Button>
    </div>
  )
}

const ButtonRemove = ({ onClick }) => {
  return (
    <div className='btn-list'>
      <Button onClick={onClick}>
        REMOVE FROM MY LIST
      </Button>
    </div>
  )
}

class TvshowDetailPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      added: false
    }
  }
 
  componentDidMount() {
    const { payload } = this.props.location.state
    this.props.fetchTvshowDetail(payload)
  }  

  handleWatchTrailer = (url) => {
    window.location.href = `${config.utubeURL}${url}`
  }

  handleAddList = (data) => {
    const arrData = []
    arrData.push(data)
    if(_.isEmpty(localStorage.getItem('tvlist'))) {
      localStorage.setItem('tvlist', JSON.stringify(arrData))
      this.setState({ added: true })
    } else {
      const store = JSON.parse(localStorage.getItem('tvlist'))
      store.push(data)
      localStorage.setItem('tvlist', JSON.stringify(store))
      this.setState({ added: true })
    }
  }

  handleRemoveList = (payload) => {
    const data = JSON.parse(localStorage.getItem('tvlist'))
    if(data.length > 1) {
      data.forEach((item, idx) => {
        if(item.name === payload.name) {
          const index = idx
          data.splice(index, index)
          localStorage.setItem('tvlist', JSON.stringify(data))
          this.setState({ added: false })
        }
      })
    } else {
      localStorage.removeItem('tvlist')
      this.setState({ added: false })
    }
  }
    
  render() {
    // set data here
    const { tvshowState } = this.props    
    const payload = !_.isEmpty(tvshowState.item) ? tvshowState.item : []
    const genrePayload = !_.isEmpty(payload) ? payload.genres.map(genres => genres.name) : []
    const datePayload = !_.isEmpty(payload) ? payload.first_air_date : {}
    const titlePayload = !_.isEmpty(payload) ? payload.name : {}

    // logic here
    const genre = genrePayload.toString().replace(/,/g, "  |   ")
    const releaseDate = !_.isEmpty(datePayload) ? datePayload.substring(0,4) : ''
    const title = !_.isEmpty(titlePayload) ? titlePayload.toUpperCase() : ''
    const season = !_.isEmpty(payload.seasons) ? payload.seasons.length : []
    const store = localStorage.getItem('tvlist') ? JSON.parse(localStorage.getItem('tvlist')) : []
    const added = []
    if(!_.isEmpty(store)) {
      store.forEach(data => {
        if(data.name === payload.name) {
          added.push(data.name)
        }
      })
    }
    const button = !_.isEmpty(added) || this.state.added ? <ButtonRemove onClick={() => this.handleRemoveList(payload)} /> : <ButtonAddList onClick={() => this.handleAddList(payload)} />

    // logic not allowed in return!
    return (
      <div className='detail-container'>
        <NavigationBar tvshow="active" />
        <div>
          <img 
            className='bg-detail'
            src={`${config.backdropURL}${payload.backdrop_path}`} 
            width='100%'
            alt='' 
          />
        </div>
        <div className='card-img-overlay'>
          <div className='content-detail-container'>
            <Row className='pb-5'>
              <Col xs={24} sm={12} md={8} lg={8} xl={8} className='text-center'>
                <div className='pl-2 pr-2 pb-4'>
                  <img 
                    className='poster-detail'
                    src={`${config.posterURL}${payload.poster_path}`}
                    alt=''
                    width='80%'
                  />
                </div>
              </Col>
              <Col xs={24} sm={12} md={10} lg={10} xl={10}>
                <div>
                  <div>
                    <p>{season} - S E A S O N S | {releaseDate.toString()}</p>
                  </div>
                  <div className='pb-5'>
                    <h1>{title}</h1>
                  </div>
                  <div>
                    <p><b>{genre}</b></p>
                  </div>
                  <div className='pb-5'>
                    <p>{payload.overview}</p>
                  </div>
                  <Row>
                    <Col xs={24} sm={24} md={24} lg={12} xl={12} className='pb-3'>
                      <div className='btn-trailer'>
                        <Button onClick={() => this.handleWatchTrailer(payload.name)}>
                          WATCH TRAILER
                        </Button>
                      </div>
                    </Col>
                    <Col xs={24} sm={24} md={24} lg={12} xl={12} className='pb-2'>
                      {button}
                    </Col>
                  </Row>
                </div>
              </Col>
            </Row>

            <div className='pl-2 pr-2 pb-1'>
              <div>
                <h4>Season</h4>
              </div>
              <div>
                <EpisodeList payload={payload.seasons}/>
              </div>
            </div>
          </div>

          <Footer />
        </div>
      </div>
    )
  }
}
const mapStateToProps = state => ({
    tvshowState: state.detailTvshowReducer
})

const mapDispatchToProps = dispatch => ({
    fetchTvshowDetail: (idx) => dispatch(ACTIONS.fetchTvshowDetail(idx))
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(TvshowDetailPage))