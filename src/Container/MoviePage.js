import React, { Component } from 'react'
import { withRouter } from 'react-router'
import _ from 'lodash'

// Redux
import { connect } from 'react-redux'
import * as ACTION_POP from '../Redux/action/moviePopAction'
import * as ACTION_PLAYING from '../Redux/action/moviePlayingAction'
import * as ACTION_UPCOMING from '../Redux/action/movieUpComingAction'
import * as ACTION_GENRE from '../Redux/action/genreAction'

// Comp
import NavigationBar from '../Components/navigation/NavigationBar'
import CategoryList from '../Components/list/slicker/CategoryList'
import MovieList from '../Components/list/slicker/MovieList'
import Footer from '../Components/Footer'

// Antd
import { Row, Col } from 'antd'
import MyMovieList from '../Components/list/slicker/MyMovieList'

class MoviePage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      genre: 28 // set default genre as action
    }
  }

  componentDidMount() {
    window.scrollTo(0,0)
    this.props.fetchMoviesPop()
    this.props.fetchMoviesPlaying()
    this.props.fetchMoviesUpComing()    
    this.props.fetchGenre()
  }  

  handleGenreClick = (id) => {
    this.setState({ genre: id })
  }

  handleMoreClick = (path) => {
    this.props.history.push(`/movie/${path}`)
  }

  _renderMylist = () => {
    const movieList = JSON.parse(localStorage.getItem('movielist'))
    
    return (
      <div className='pl-4 pr-4 pb-4'>
        <div>
          <h4>My List</h4>
        </div>
        <div>
          <MyMovieList payload={movieList} />
        </div>
      </div>
    )
  }

  render() {
    // set data here
    const { moviePopState, moviePlayingState, movieUpComingState, genreState } = this.props
    const payloadPop = !_.isEmpty(moviePopState.items) ? moviePopState.items.results : []
    const payloadComing = !_.isEmpty(movieUpComingState.items) ? movieUpComingState.items.results : []
    const payloadPlaying = !_.isEmpty(moviePlayingState.items) ? moviePlayingState.items.results : []
    const genrePayload = !_.isEmpty(genreState.data) ? genreState.data : []
    const genreArr = []
    const arr = []

    // logic here
    payloadPop.forEach(items => {
      const genre = items.genre_ids
      if(genre.includes(this.state.genre)) {
        arr.push(items)
      }
    })

    payloadPlaying.forEach(items => {
      const genre = items.genre_ids
      if(genre.includes(this.state.genre)) {
        arr.push(items)
      }
    })

    payloadComing.forEach(items => {
      const genre = items.genre_ids
      if(genre.includes(this.state.genre)) {
        arr.push(items)
      }
    })

    genrePayload.forEach(items => {
      if(_.isEqual(items.name, 'Action') || _.isEqual(items.name, 'Adventure') || _.isEqual(items.name, 'Fantasy') || _.isEqual(items.name, 'Drama') || _.isEqual(items.name, 'Horror') ) {
        genreArr.push(items)
      }
    })

    const mylist = !_.isEmpty(localStorage.getItem('movielist')) ? this._renderMylist() : null

    // logic not allowed in return!
    return(
      <div className='movie-container'>
        <NavigationBar movie="active"/>
        <div className="pl-4 pr-4 pt-5 pb-4">
          <div className="header-title">
            <Row justify='space-between'>
              <Col xs={16} sm={14} md={20} lg={16} xl={20}>
                <div>
                  <h4>Now Playing</h4>
                </div>
              </Col>
              <Col xs={8} sm={4} md={4} lg={4} xl={4}>
                <div className="text-link">
                  <span onClick={() => this.handleMoreClick('nowplaying')}>Show More</span>
                </div>
              </Col>
            </Row>            
          </div>
          <div className='pl-4 pr-4 pb-4'>
            <MovieList payload={payloadPlaying} />
          </div>
        </div>

        <div className='pl-4 pr-4'>
          <div className='pb-1'>
            <h4>Browse by category</h4>
          </div>
          <div className='pb-4'>
            <Row>
            {
              genreArr.map((data, idx) => {
                return (
                  <Col xs={10} sm={10} md={4} lg={4} xl={2} 
                    key={idx} span={3} onClick={() => this.handleGenreClick(data.id)}
                  >
                    <div className='category-container'>
                      <span>{data.name}</span>
                    </div>
                  </Col>
                )
              })
            }
            </Row>
          </div>
          <div className='pl-4 pr-4 pt-1 pb-3'>
            <CategoryList payload={arr} />
          </div>
        </div>

        <div className="pl-4 pr-4 pb-4">
          <div className="header-title">
            <Row>
              <Col xs={16} sm={14} md={20} lg={16} xl={20}>
                <div>
                  <h4>Popular Movie</h4>
                </div>
              </Col>
              <Col xs={8} sm={4} md={4} lg={4} xl={4}>
                <div className="text-link">
                  <span onClick={() => this.handleMoreClick('popular')}>Show More</span>
                </div>
              </Col>
            </Row>
          </div>
          <div className='pl-4 pr-4 pb-4 pt-1'>
            <MovieList payload={payloadPop} />
          </div>
        </div>

        <div>
          {mylist}
        </div>

        <div className="pl-4 pr-4 pb-4">
          <div className="header-title">
            <Row>
              <Col xs={16} sm={14} md={20} lg={16} xl={20}>
                <div>
                  <h4>Coming Soon</h4>
                </div>
              </Col>
              <Col xs={8} sm={4} md={4} lg={4} xl={4}>
                <div className="text-link">
                  <span onClick={() => this.handleMoreClick('upcoming')}>Show More</span>
                </div>
              </Col>
            </Row>
          </div>
          <div className='pl-4 pr-4 pb-4 pt-1'>
            <MovieList payload={payloadComing} />
          </div>
        </div>
      
        <Footer />
      </div>
    )
  }
}

const mapStateToProps = state => ({
    moviePopState: state.moviePopReducer,
    moviePlayingState: state.moviePlayingReducer,
    movieUpComingState: state.movieUpComingReducer,
    genreState: state.genreReducer
})

const mapDispatchToProps = dispatch => ({
  fetchMoviesPop: () => dispatch(ACTION_POP.fetchMoviesPop()),
  fetchMoviesPlaying: () => dispatch(ACTION_PLAYING.fetchMoviesPlaying()),
  fetchMoviesUpComing: () => dispatch(ACTION_UPCOMING.fetchMoviesUpComing()),
  fetchGenre: () => dispatch(ACTION_GENRE.fetchGenre())
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MoviePage))