import React, { Component } from 'react'
import _ from 'lodash'

// Redux
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import * as ACTION from '../Redux/action/searchAction'

// Component
import CardMovie from '../Components/card/CardMovie'
import NavigationBar from '../Components/navigation/NavigationBar'
import LoadingSpin from '../Components/loader/LoadingSpin'
import MovieMoreList from '../Components/list/MovieMoreList'

class SearchPage extends Component {

  componentDidMount() {
    const { query } = this.props.location.state
    this.props.getSearchQuery(query)
  }

  render() {
    // set data here
    const { searchState } = this.props
    const { query } = this.props.location.state
    const payload = !_.isEmpty(searchState.items) ? searchState.items.results : []    
    
    // logic here
    const cardResult = searchState.fetching ? <LoadingSpin /> : <MovieMoreList payload={ payload } />
    const displayResult = !_.isEmpty(payload) ? cardResult : "Empty"

    // logic not allowed in return!
    return(
      <div>
        <NavigationBar />
        <div className="container" style={{marginTop: '30px'}}>
          <div>
            <p>results of {query}...</p>
          </div>
          { displayResult }
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  searchState: state.searchReducer
})

const mapDispatchToProps = dispatch => ({
  getSearchQuery: (query) => dispatch(ACTION.getSearchQuery(query))
})

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(SearchPage))